import { Form, Input, Button } from 'antd';
import { useCreate } from 'hooks/useCreate';
import { Team } from 'lib/models';

export const TeamCreate = () => {
    const [form] = Form.useForm();
    const [createTeam, creating] = useCreate<Team>('teams');

    const handleFinish = (values: Team) => {
        createTeam(values);
    }

    return (
        <Form name="Team-create" form={form} onFinish={handleFinish}>
            <Form.Item name="name" label="Team name" rules={[{required: true}]}>
                <Input />
            </Form.Item>
            <Form.Item>
                <Button disabled={creating} type="primary" htmlType="submit">Create</Button>
            </Form.Item>
        </Form>
    )
}
