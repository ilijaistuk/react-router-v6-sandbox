import { Button } from "antd";
import { useGetAll } from "hooks/useGetAll";
import { Team } from "lib/models";
import { useNavigate } from "react-router"
import { Link } from "react-router-dom";

export const TeamsList = () => {
    const navigate = useNavigate();
    const { loading, data } = useGetAll<Team>('teams');

    if (loading) return <span>Loading...</span>;

    return (
        <div>
            <Button onClick={() => navigate('create')}>Create new Team</Button>
            {data?.map((Team) => (
                <div key={Team.id}>
                    <h2><Link to={`${Team.id}`}>{Team.name}</Link></h2>
                </div>
            ))}
        </div>
    )
}
