import React from "react";
import { Routes, Route } from 'react-router-dom';
import { TeamCreate } from "./TeamCreate";
import { TeamsList } from "./TeamList";
import { TeamView } from "./TeamView";

export const TeamsRoutes = () => {

    return (
        <Routes>
            <Route path="/" element={<TeamsList />} />
            <Route path=":id" element={<TeamView />} />
            <Route path="create" element={<TeamCreate />} />
        </Routes>
    )
}
