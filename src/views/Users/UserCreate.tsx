import { Form, Input, Button } from 'antd';
import { useCreate } from 'hooks/useCreate';
import { User } from 'lib/models';

export const UserCreate = () => {
    const [form] = Form.useForm();
    const [createUser, creating] = useCreate<User>('users');

    const handleFinish = (values: User) => {
        createUser(values);
    }

    return (
        <Form name="User-create" form={form} onFinish={handleFinish}>
            <Form.Item name="name" label="User name" rules={[{required: true}]}>
                <Input />
            </Form.Item>
            <Form.Item>
                <Button disabled={creating} type="primary" htmlType="submit">Create</Button>
            </Form.Item>
        </Form>
    )
}
