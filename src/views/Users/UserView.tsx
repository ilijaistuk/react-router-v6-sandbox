import { Breadcrumb } from "antd"
import { useGetOne } from "hooks/useGetOne"
import { User, ViewParams } from "lib/models";
import { useMemo } from "react";
import { Link, useParams } from "react-router-dom"

export const UserView = () => {
    const params = useParams<keyof ViewParams>();
    const userId = useMemo(() => parseInt(params.id ?? ''), [params.id]);
    const { loading, item } = useGetOne<User>('users', userId);

    if (loading) return <span>Loading...</span>;
    if (!item) return <span>Record not found</span>

    return (
        <div>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link to="../../dashboard">Dashboard</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to="..">User List</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    {item.id}
                </Breadcrumb.Item>
            </Breadcrumb>
            <div>
                <h1>{item.name}</h1>
            </div>
        </div>
    )
}
