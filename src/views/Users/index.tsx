import React, { Suspense } from "react";
import { Routes, Route } from 'react-router-dom';
import { UserCreate } from "./UserCreate";
import { UsersList } from "./UsersList";
import { UserView } from "./UserView";

export const UsersRoutes = () => {

    return (
        <Routes>
            <Route path="/" element={<UsersList />} />
            <Route path=":id" element={<UserView />} />
            <Route path="create" element={<UserCreate />} />
        </Routes>
    )
}
