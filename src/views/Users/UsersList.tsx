import { Button } from "antd";
import { useGetAll } from "hooks/useGetAll";
import { User } from "lib/models";
import { useNavigate } from "react-router"
import { Link } from "react-router-dom";

export const UsersList = () => {
    const navigate = useNavigate();
    const { loading, data } = useGetAll<User>('users');

    if (loading) return <span>Loading...</span>;

    return (
        <div>
            <Button onClick={() => navigate('create')}>Create new user</Button>
            {data?.map((user) => (
                <div key={user.id}>
                    <h2><Link to={`${user.id}`}>{user.name}</Link></h2>
                </div>
            ))}
        </div>
    )
}
