import React from "react";
import { Routes, Route } from 'react-router-dom';
import { PostCreate } from "./PostCreate";
import { PostsList } from "./PostsList";
import { PostView } from "./PostView";

export const PostsRoutes = () => {

    return (
        <Routes>
            <Route path="/" element={<PostsList />} />
            <Route path=":id" element={<PostView />} />
            <Route path="create" element={<PostCreate />} />
        </Routes>
    )
}
