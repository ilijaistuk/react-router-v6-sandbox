import { Button } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { useGetAll } from "../../hooks/useGetAll"
import { Post } from "../../lib/models"

export const PostsList = () => {
    const navigate = useNavigate();
    const { loading, data  } = useGetAll<Post>('posts');

    if (loading) return <span>Loading...</span>;

    return (
        <div>
            <Button onClick={() => navigate('create')}>Create new Post</Button>
            {data?.map((post) => (
                <div key={post.id}>
                    <h2><Link to={`${post.id}`}>{post.name}</Link></h2>
                    <p>{post.content}</p>
                </div>
            ))}
        </div>
    )
}
