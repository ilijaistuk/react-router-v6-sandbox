import { Form, Input, Button } from 'antd';
import { useCreate } from 'hooks/useCreate';
import { Post } from 'lib/models';

export const PostCreate = () => {
    const [form] = Form.useForm();
    const [createPost, creating] = useCreate<Post>('posts');

    const handleFinish = (values: Post) => {
        createPost(values);
    }

    return (
        <Form name="post-create" form={form} onFinish={handleFinish}>
            <Form.Item name="name" label="Post name" rules={[{required: true}]}>
                <Input />
            </Form.Item>
            <Form.Item name="content" label="Content" rules={[{required: true}]}>
                <Input.TextArea />
            </Form.Item>
            <Form.Item>
                <Button disabled={creating} type="primary" htmlType="submit">Create</Button>
            </Form.Item>
        </Form>
    )
}
