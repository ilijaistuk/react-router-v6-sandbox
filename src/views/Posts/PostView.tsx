import { useGetOne } from "hooks/useGetOne";
import { Post, ViewParams } from "lib/models";
import { useMemo } from "react";
import { useParams } from "react-router-dom";

export const PostView = () => {
    const params = useParams<keyof ViewParams>();
    const postId = useMemo(() => parseInt(params.id ?? ''), [params.id]);
    const { loading, item } = useGetOne<Post>('posts', postId);

    if (loading) return <span>Loading...</span>;
    if (!item) return <span>Record not found</span>

    return (
        <div>
            <h1>{item.name}</h1>
            <p>
                {item.content}
            </p>
        </div>
    )
}
