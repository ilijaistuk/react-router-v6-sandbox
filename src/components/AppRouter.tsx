import { ReactElement } from "react"
import { useRoutes } from "react-router-dom"

export type RoutesArray = {path: string, element: ReactElement}[]

interface AppRouterProps {
    routes: RoutesArray
}

export const AppRouter: React.VFC<AppRouterProps> = ({routes}) => {
    const router = useRoutes(routes);

    return router;
}
