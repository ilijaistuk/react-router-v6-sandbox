import { ReactElement } from "react";

export interface AppResourceProps {
    name: string;
    list?: ReactElement;
    create?: ReactElement;
    view?: ReactElement;
}

export const AppResource: React.VFC<AppResourceProps> = (props) => {

    return null
}
