import { Layout, Menu } from 'antd';
import { Content, Header } from 'antd/lib/layout/layout';
import React, { ReactElement, useMemo } from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate, Link } from 'react-router-dom';
import { AppResourceProps } from './AppResource';
import { AppRouter, RoutesArray } from './AppRouter';

interface AppAppProps {
    children: ReactElement<AppResourceProps> | ReactElement<AppResourceProps>[]
}

export const AppApp = ({children}: AppAppProps) => {
    const routes: RoutesArray = useMemo(() => {
        const loadedRoutes: RoutesArray = [];
        React.Children.map(children, ({props}) => {
            const name = props.name;

            if (props.list) {
                loadedRoutes.push({ path: `/${name}`, element: props.list});
            }
            if (props.view) {
                loadedRoutes.push({ path: `/${name}/:id`, element: props.view});
            }
            if (props.create) {
                loadedRoutes.push({ path: `/${name}/create`, element: props.create});
            }
        });

        return loadedRoutes;
    }, [children]);

    return (
        <Router>
            <Layout className="layout">
                <Header>
                    <Menu theme="dark" mode="horizontal">
                        <Menu.Item key="menu-dashboard"><Link to="/dashboard" />Dashboard</Menu.Item>
                        <Menu.Item key="menu-teams"><Link to="/teams" />Teams</Menu.Item>
                        <Menu.Item key="menu-posts"><Link to="/posts" />Posts</Menu.Item>
                        <Menu.Item key="menu-users"><Link to="/users" />Users</Menu.Item>
                    </Menu>
                </Header>
                <Content>
                    <AppRouter routes={routes} />
                </Content>
            </Layout>
        </Router>
    )
}
