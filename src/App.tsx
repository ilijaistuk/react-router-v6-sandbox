import { BrowserRouter as Router, Routes, Route, Navigate, Link, NavLink } from 'react-router-dom';
import { Dashboard } from './views/Dashboard';
import { PostsRoutes } from './views/Posts';
import { TeamsRoutes } from './views/Teams';
import { UsersRoutes } from './views/Users';

import { Layout, Menu, Breadcrumb } from 'antd';
import { AppApp } from 'components/AppApp';
import { AppResource } from 'components/AppResource';
import { TeamsList } from 'views/Teams/TeamList';
import { TeamView } from 'views/Teams/TeamView';
import { TeamCreate } from 'views/Teams/TeamCreate';
import { PostsList } from 'views/Posts/PostsList';
import { PostView } from 'views/Posts/PostView';
import { PostCreate } from 'views/Posts/PostCreate';
import { UsersList } from 'views/Users/UsersList';
import { UserView } from 'views/Users/UserView';
import { UserCreate } from 'views/Users/UserCreate';

const { Header, Content } = Layout;

function App() {
  return (
      <Router>
        <Layout className="layout">
          <Header>
            <Menu theme="dark" mode="horizontal">
              <Menu.Item key="menu-dashboard"><Link to="/dashboard" />Dashboard</Menu.Item>
              <Menu.Item key="menu-teams"><Link to="/teams" />Teams</Menu.Item>
              <Menu.Item key="menu-posts"><Link to="/posts" />Posts</Menu.Item>
              <Menu.Item key="menu-users"><Link to="/users" />Users</Menu.Item>
            </Menu>
          </Header>
          <Content>
            <Routes>
              <Route path="/" element={<Navigate to="/dashboard" replace />} />
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="/teams/*" element={<TeamsRoutes />} />
              <Route path="/posts/*" element={<span>Ted</span>} />
              <Route path="/users/*" element={<UsersRoutes />} />
            </Routes>
          </Content>
        </Layout>
      </Router>
  );
}




























// function DynamicApp() {
//   return (
//     <AppApp>
//       <AppResource
//         name="dashboard"
//         list={<Dashboard />} />
//       <AppResource
//         name="teams"
//         list={<TeamsList />}
//         view={<TeamView />}
//         create={<TeamCreate />} />
//       <AppResource
//         name="posts"
//         list={<PostsList />}
//         view={<PostView />}
//         create={<PostCreate />} />
//       <AppResource
//         name="users"
//         list={<UsersList />}
//         view={<UserView />}
//         create={<UserCreate />} />
//     </AppApp>
//   )
// }

export default App;
