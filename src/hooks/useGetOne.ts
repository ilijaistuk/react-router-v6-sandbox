import axios from "axios";
import { useEffect, useState } from "react";
import { BaseModel } from "../lib/models";
import { API_URL, BaseRequest, ResourceName } from "./common";

export interface ViewRequest<TModel extends BaseModel> extends BaseRequest {
    item?: TModel;
}

export function useGetOne<TModel extends BaseModel>(resource: ResourceName, id: number) {
    const [status, setStatus] = useState<ViewRequest<TModel>>({
        loading: false,
        loaded: false
    });

    useEffect(() => {
        setStatus({
            loading: true,
            loaded: false
        });

        axios.get(`${API_URL}/${resource}/${id}`).then((response) => {
            setStatus({
                loaded: true,
                loading: false,
                item: response.data
            })
        }).catch((reason) => {
            setStatus({
                loaded: false,
                loading: false,
                error: reason
            })
        });
    }, [resource, id]);

    return status;
}
