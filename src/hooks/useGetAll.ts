import axios from "axios";
import { useEffect, useState } from "react"
import { BaseModel } from "../lib/models";
import { API_URL, BaseRequest, ResourceName } from "./common";

export interface ListRequest<TModel extends BaseModel> extends BaseRequest {
    data?: TModel[];
}

export function useGetAll<TModel extends BaseModel>(resource: ResourceName) {
    const [status, setStatus] = useState<ListRequest<TModel>>({
        loading: false,
        loaded: false
    });

    useEffect(() => {
        setStatus({
            loading: true,
            loaded: false
        });

        axios.get(`${API_URL}/${resource}`).then((response) => {
            setStatus({
                loaded: true,
                loading: false,
                data: response.data
            });
        }).catch((reason) => {
            setStatus({
                loaded: false,
                loading: false,
                error: reason
            });
        });
    }, [resource]);

    return status;
}
