export interface BaseRequest {
    loading: boolean;
    loaded: boolean;
    error?: string;
}

export type ResourceName = "teams" | "posts" | "users";

export const API_URL = 'http://localhost:5000';
