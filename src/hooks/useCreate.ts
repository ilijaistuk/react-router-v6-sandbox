import axios, { AxiosResponse } from "axios";
import { BaseModel } from "lib/models";
import { useCallback, useState } from "react";
import { useNavigate } from "react-router-dom";
import { API_URL, ResourceName } from "./common";

export function useCreate<TModel extends BaseModel>(resource: ResourceName): [Function, boolean] {
    const [creating, setCreating] = useState(false);

    const navigate = useNavigate();
    const create = useCallback((object: TModel) => {
        if (object.id) throw new Error('use useEdit to edit items');

        setCreating(true)

        axios.post(`${API_URL}/${resource}`, object).then(({data}: AxiosResponse<BaseModel>) => {
            setCreating(false);
            navigate(`../${data.id}`);
        });
    }, [resource]);

    return [create, creating];
}
