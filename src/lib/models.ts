export interface BaseModel {
    id?: number;
}

export interface Team extends BaseModel {
    name: string;
}

export interface Post extends BaseModel {
    name: string;
    content: string;
}

export interface User extends BaseModel {
    name: string;
}

export type ViewParams = {
    id: string
} ;
